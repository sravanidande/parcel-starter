// tslint:disable

// 30seconds of code

export const predFn: (x: number) => boolean = x => {
  return x > 1
}

export const all: (arr: number[], fn: (x: number) => boolean) => boolean = (
  arr,
  fn,
) => {
  for (let i = 0; i < arr.length; i += 1) {
    if (!fn(arr[i])) {
      return false
    }
  }
  return true
}

export const allEqual: (arr: number[]) => boolean = arr => {
  for (let i = 1; i < arr.length; i += 1) {
    if (arr[i] !== arr[0]) {
      return false
    }
  }
  return true
}

export const pred1 = (x: number) => x % 2 === 0

export const atleastOne: (
  arr: number[],
  fn: (x: number) => boolean,
) => boolean = (arr, fn) => {
  for (let i = 0; i < arr.length; i += 1) {
    if (fn(arr[i])) {
      return true
    }
  }
  return false
}

export const drop: (arr: number[], n: number) => number[] = (arr, n) => {
  return arr.slice(n)
}

export const dropRight: (arr: number[], n: number) => number[] = (arr, n) => {
  return arr.slice(0, -n)
}

export const filterNonUnique: (arr: number[]) => number[] = arr => {
  const result: number[] = []
  for (let i = 0; i < arr.length; i += 1) {
    if (arr.indexOf(arr[i]) === arr.lastIndexOf(arr[i])) {
      result.push(arr[i])
    }
  }
  return result
}

export const pred2 = (x: number) => x % 2 === 1

export const findLast: (arr: number[], f: (x: number) => boolean) => any = (
  arr,
  f,
) => {
  const result: number[] = []
  for (let i = 0; i < arr.length; i += 1) {
    if (f(arr[i])) {
      result.push(arr[i])
    }
  }
  return result.pop()
}

export const exceptLast: (arr: number[]) => number[] = arr => {
  return arr.slice(0, -1)
}

export const initializeArrayWithValues: (x: number, y: number) => number[] = (
  x,
  y,
) => {
  const result = []
  for (let i = 0; i < x; i += 1) {
    result.push(y)
  }
  return result
}

export const intersection: (ar1: number[], ar2: number[]) => number[] = (
  ar1,
  ar2,
) => {
  const result = []
  for (let i = 0; i < ar2.length; i += 1) {
    if (ar1.includes(ar2[i])) {
      result.push(ar2[i])
    }
  }
  return result
}

export const remove: (arr: number[], f: (x: number) => boolean) => number[] = (
  arr,
  f,
) => {
  const result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (f(arr[i])) {
      result.push(arr[i])
    }
  }
  return result
}

export const tail: (arr: number[]) => number[] = arr => {
  return arr.length > 1 ? arr.slice(1) : arr
}

export const uniqueElements: (arr: number[]) => number[] = arr => {
  const result: number[] = []
  for (let i = 0; i < arr.length; i += 1) {
    if (result.indexOf(arr[i]) === -1) {
      result.push(arr[i])
    }
  }
  return result
}

export const none: (arr: number[], f: (x: number) => boolean) => boolean = (
  arr,
  f,
) => {
  for (let i = 0; i < arr.length; i += 1) {
    if (!f(arr[i])) {
      return true
    }
  }
  return false
}

export const nthElement: (arr: string[], n: number) => string = (arr, n) => {
  return arr.splice(n, 1)[0]
}

export const offset: (arr: number[], off: number) => number[] = (arr, off) => {
  return [...arr.slice(off, arr.length), ...arr.slice(0, off)]
}

export const reject: <T>(arr: T[], f: (x: T) => boolean) => T[] = (arr, f) => {
  const result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (!f(arr[i])) {
      result.push(arr[i])
    }
  }
  return result
}

export const similarity: (ar1: number[], ar2: number[]) => number[] = (
  ar1,
  ar2,
) => {
  const result = []
  for (let i = 0; i < ar2.length; i += 1) {
    if (ar2.includes(ar1[i])) {
      result.push(ar1[i])
    }
  }
  return result
}

export const union: (ar1: number[], ar2: number[]) => number[] = (ar1, ar2) => {
  const result = [...ar1, ...ar2]
  return uniqueElements(result)
}

export const takeWhile: (
  arr: number[],
  f: (x: number) => boolean,
) => number[] = (arr, f) => {
  const result = []
  for (let i = 0; i < arr.length; i++) {
    if (!f(arr[i])) {
      result.push(arr[i])
    }
  }
  return result
}

export const compact: (arr: any[]) => any[] = arr => {
  let result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (Boolean(arr[i]) !== false) {
      result.push(arr[i])
    }
  }
  return result
}

export const countOccurences: (arr: number[], value: number) => number = (
  arr,
  value,
) => {
  let count = 0
  for (let i = 0; i < arr.length; i += 1) {
    if (arr[i] === value) {
      count += 1
    }
  }
  return count
}

export const filterFalsy: (arr: any[]) => any[] = arr => {
  let result = []
  for (let i = 0; i < arr.length; i += 1) {
    if (Boolean(arr[i])) {
      result.push(arr[i])
    }
  }
  return result
}

export const initializeArrayWithRangeRight: (
  start: number,
  stop: number,
  step: number,
) => number[] = (start, stop, step = 2) => {
  let result = []
  for (let i = start; i >= stop; i -= step) {
    result.push(i)
  }
  return result
}

export const take: (arr: number[], n: number) => number[] = (arr, n) => {
  let result = []
  if (arr.length < n) {
    return arr
  }
  for (let i = 0; i < n; i += 1) {
    result.push(arr[i])
  }
  return result
}

export const takeRight: (arr: number[], n: number) => number[] = (arr, n) => {
  return arr.slice(arr.length - n, arr.length)
}

export const takeRightWhile: (
  arr: number[],
  f: (x: number) => boolean,
) => number[] = (arr, f) => {
  let result = []
  for (let i = arr.length; i > 0; i -= 1) {
    if (!f(arr[i])) {
      result.push(arr[i])
    }
  }
  return result
}

export const longestItem: (arr: string[]) => string = arr => {
  let max = arr[0]
  for (let i = 1; i < arr.length; i += 1) {
    if (max.length < arr[i].length) max = arr[i]
  }
  return max
}

export const sample: (arr: number[]) => number = arr => {
  return arr[Math.floor(Math.random() * arr.length)]
}

export const uniqueSymmetricArray: (
  arr1: number[],
  arr2: number[],
) => number[] = (arr1, arr2) => {
  const result = []
  for (let i = 0; i < arr2.length; i += 1) {
    if (!arr1.includes(arr2[i]) || !arr2.includes(arr1[i])) {
      result.push(arr1[i], arr2[i])
    }
  }
  return result
}
