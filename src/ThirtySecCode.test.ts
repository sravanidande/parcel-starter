import {
  all,
  allEqual,
  atleastOne,
  compact,
  countOccurences,
  drop,
  dropRight,
  exceptLast,
  filterFalsy,
  filterNonUnique,
  findLast,
  initializeArrayWithRangeRight,
  initializeArrayWithValues,
  intersection,
  longestItem,
  none,
  nthElement,
  offset,
  reject,
  remove,
  similarity,
  tail,
  take,
  takeRight,
  takeWhile,
  union,
  uniqueElements,
  uniqueSymmetricArray,
} from './ThirtySecCode'

export const pred: (x: number) => boolean = x => {
  return x > 1
}
export const pred1 = (x: number) => x % 2 === 0
export const pred2 = (x: number) => x % 2 === 1
export const predF = (x: number) => x === 2
export const predF1 = (x: string) => x.length > 4
export const predF2 = (x: number) => x >= 3
export const predF3 = (x: number) => x < 3

it('allFunction', () => {
  expect(all([4, 2, 3], pred)).toBeTruthy()
  expect(all([1, 2, 3], pred)).toBeFalsy()
  expect(all([1, 5, 3], pred2)).toBeTruthy()
  expect(all([0, 1, 3], pred1)).toBeFalsy()
})

it('allEqual', () => {
  expect(allEqual([1, 1, 1, 1])).toBeTruthy()
  expect(allEqual([1, 2, 3, 4, 5, 6])).toBeFalsy()
  expect(allEqual([1, 2, 2])).toBeFalsy()
  expect(allEqual([0, 0, 0])).toBeTruthy()
})

it('atleastOne', () => {
  expect(atleastOne([0, 1, 2], pred)).toBeTruthy()
  expect(atleastOne([0, 1, 1], pred)).toBeFalsy()
  expect(atleastOne([1, 3, 2], pred1)).toBeTruthy()
  expect(atleastOne([2, 4, 3], pred2)).toBeTruthy()
  expect(atleastOne([2, 4, 6], pred2)).toBeFalsy()
})

it('drop function', () => {
  expect(drop([1, 2, 3], 1)).toEqual([2, 3])
  expect(drop([1, 2, 3], 2)).toEqual([3])
  expect(drop([1, 2, 3], 42)).toEqual([])
})

it('dropRight', () => {
  expect(dropRight([1, 2, 3], 1)).toEqual([1, 2])
  expect(dropRight([1, 2, 3], 2)).toEqual([1])
  expect(dropRight([1, 2, 3], 42)).toEqual([])
})

it('filterNonUnique', () => {
  expect(filterNonUnique([1, 2, 2, 3, 4, 4, 5])).toEqual([1, 3, 5])
})

it('findLast', () => {
  expect(findLast([1, 2, 3, 4], pred2)).toEqual(3)
})

it('except last element in an array', () => {
  expect(exceptLast([1, 2, 3, 4])).toEqual([1, 2, 3])
})

it('print y number x times', () => {
  expect(initializeArrayWithValues(5, 2)).toEqual([2, 2, 2, 2, 2])
})

it('print common means intersection', () => {
  expect(intersection([1, 2, 3], [4, 2, 3])).toEqual([2, 3])
})

it('prints if given pred is true as remove fn', () => {
  expect(remove([1, 2, 3, 4], pred1)).toEqual([2, 4])
})

it('prints all elements except first', () => {
  expect(tail([1, 2, 3])).toEqual([2, 3])
  expect(tail([1])).toEqual([1])
})

it('unique elements', () => {
  expect(uniqueElements([1, 2, 2, 3, 4, 4, 5])).toEqual([1, 2, 3, 4, 5])
})

it('none', () => {
  expect(none([0, 1, 3, 0], predF)).toBeTruthy()
})

it('nthElement', () => {
  expect(nthElement(['a', 'b', 'c'], 1)).toEqual('b')
  expect(nthElement(['a', 'b', 'b'], -3)).toEqual('a')
})

it('offset', () => {
  expect(offset([1, 2, 3, 4, 5], 2)).toEqual([3, 4, 5, 1, 2])
  expect(offset([1, 2, 3, 4, 5], -2)).toEqual([4, 5, 1, 2, 3])
})

it('reject', () => {
  expect(reject([1, 2, 3, 4, 5], pred1)).toEqual([1, 3, 5])
  expect(reject(['Apple', 'Peer', 'Kiwi', 'Banana'], predF1)).toEqual([
    'Peer',
    'Kiwi',
  ])
})

it('similarity', () => {
  expect(similarity([1, 2, 3], [1, 2, 4])).toEqual([1, 2])
})

it('union', () => {
  expect(union([1, 2, 3], [4, 3, 2])).toEqual([1, 2, 3, 4])
})

it('takeWhile', () => {
  expect(takeWhile([1, 2, 3, 4], predF2)).toEqual([1, 2])
})

it('compact', () => {
  expect(compact([0, 1, false, 2, '', 3, 'a', NaN, 's', 34])).toEqual([
    1,
    2,
    3,
    'a',
    's',
    34,
  ])
})

it('count occurences', () => {
  expect(countOccurences([1, 1, 2, 1, 2, 3], 2)).toEqual(2)
})

it('filterFalsy', () => {
  expect(filterFalsy(['', true, {}, false, 'sample', 1, 0])).toEqual([
    true,
    {},
    'sample',
    1,
  ])
})

it('initialize array with range right', () => {
  expect(initializeArrayWithRangeRight(9, 0, 2)).toEqual([9, 7, 5, 3, 1])
})

it('take', () => {
  expect(take([1, 2], 0)).toEqual([])
  expect(take([1, 2, 3], 5)).toEqual([1, 2, 3])
})

it('takeRight', () => {
  expect(takeRight([1, 2, 3], 2)).toEqual([2, 3])
})

it('longestItem', () => {
  expect(longestItem(['this', 'is', 'a', 'testcase'])).toEqual('testcase')
})

// it('generateRandomNumber', () => {
//   expect(sample([3, 7, 9, 11])).toEqual(11 )
// })

it('uniqueSymmetricArray', () => {
  expect(uniqueSymmetricArray([1, 2, 3], [1, 2, 4])).toEqual([3, 4])
})
